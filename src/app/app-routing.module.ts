import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherCityDetailsComponent } from './components/weather-city-details/weather-city-details.component';
import { AppComponent } from './app.component';
import { WeatherListComponent } from './components/weather-list/weather-list.component';
import { SearchListComponent } from './components/search-list/search-list.component';


const routes: Routes = [
  { path: 'cities/:cityName', component: WeatherListComponent },
  { path: '', component: WeatherListComponent },
  { path: 'search/:lon/:lat', component: SearchListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
