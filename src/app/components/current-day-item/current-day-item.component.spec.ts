import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentDayItemComponent } from './current-day-item.component';

describe('CurrentDayItemComponent', () => {
  let component: CurrentDayItemComponent;
  let fixture: ComponentFixture<CurrentDayItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentDayItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentDayItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
