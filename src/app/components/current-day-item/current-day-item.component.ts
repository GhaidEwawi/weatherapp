import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { Weather } from '../../models/weather';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-current-day-item',
  templateUrl: './current-day-item.component.html',
  styleUrls: ['./current-day-item.component.scss']
})
export class CurrentDayItemComponent {

  constructor() { }

  @Input() weather: object;

}
