import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {

  resultSet;
  searchedList;
  longtitude;
  latitude;
  searchQuery = '';

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.longtitude = position.coords.longitude;
        this.latitude = position.coords.latitude;
        this.weatherService.getNearbyCities(this.latitude, this.longtitude).subscribe(resultSet => {
          this.resultSet = resultSet;
          this.resultSet = this.resultSet.list;
          this.searchedList = this.resultSet;
        });
      });
    }
  }

  getResultSet() {

    this.searchedList = this.resultSet.filter(item => {
      return item.name.search(this.searchQuery) > -1
    })
  }

}
