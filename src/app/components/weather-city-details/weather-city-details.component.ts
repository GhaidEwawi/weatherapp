import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-weather-city-details',
  templateUrl: './weather-city-details.component.html',
  styleUrls: ['./weather-city-details.component.scss']
})
export class WeatherCityDetailsComponent implements OnInit {

  weather;
  weatherList;
  lat;
  lon;

  constructor(private weatherService: WeatherService,
              private route: ActivatedRoute,
              private router: Router) { }
              
  ngOnInit() {
    if(this.route.snapshot.paramMap.has('cityName')) {
    this.weatherService.getFiveDaysWeatherByCity(this.route.snapshot.paramMap.get('cityName')).subscribe(weather => {
      this.weather = weather;
      this.weatherList = this.weather.list;
    });
  } else {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          this.lon = position.coords.longitude;
          this.lat = position.coords.latitude;
          this.weatherService.getFiveDaysWeatherByCoordinates(this.lat, this.lon).subscribe(weather => {
            this.weather = weather;
            this.weatherList = this.weather.list;
          });
        });
      }
    }
  }

}
