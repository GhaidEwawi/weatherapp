import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss']
})
export class WeatherItemComponent implements OnInit {

  @Input() min: number;
  @Input() max: number;
  @Input() iconUrl: string;
  @Input() ind: number;

  constructor() { }

  ngOnInit() {
  }

}
