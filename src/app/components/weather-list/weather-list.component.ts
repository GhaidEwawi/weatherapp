import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Weather } from 'src/app/models/weather';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.scss']
})
export class WeatherListComponent implements OnInit {

  weather;
  today;
  weatherList;
  lat;
  lon;
  constructor(private weatherService: WeatherService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {

  if(this.route.snapshot.paramMap.has('cityName')) {
    this.weatherService.getLocalWeatherByCityName(this.route.snapshot.paramMap.get('cityName')).subscribe(today => {
      this.today = today;
    });
    this.weatherService.getFiveDaysWeatherByCity(this.route.snapshot.paramMap.get('cityName')).subscribe(weather => {
      this.weather = weather;
      this.weatherList = this.weather.list;
    });

  } else {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          this.lon = position.coords.longitude;
          this.lat = position.coords.latitude;
          this.weatherService.getFiveDaysWeatherByCoordinates(this.lat, this.lon).subscribe(weather => {
            this.weather = weather;
            this.weatherList = this.weather.list;
          });
          this.weatherService.getLocalWeather(this.lat, this.lon).subscribe(today => {
            this.today = today;
          });
        });
      }
    }
  }

}
