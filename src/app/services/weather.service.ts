import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Weather } from '../models/weather';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  mainParams = {
    appid: environment.weatherServiceAppId,
    units: 'metric'
  };

  getLocalWeatherByCityName(cityName: string) {
    let customParams = {
      q: cityName
    };
    return this.sendRequest(customParams, 'weather');
  }

  getLocalWeather(lat: number, lon: number) {
    let customParams = {
      lat: lat,
      lon: lon
    };
    return this.sendRequest(customParams, 'weather');
  }

  getFiveDaysWeatherByCoordinates(latitude: number, longtitude: number) {
    let customParams = {
      lon: longtitude,
      lat: latitude
    };
    return this.sendRequest(customParams, 'forecast');
  }

  getFiveDaysWeatherByCity(q:string) {
    let customParams = {
      q: q
    };
    return this.sendRequest(customParams, 'forecast');
  }

  getNearbyCities(latitude: number, longtitude: number) {
    let customParams = {
      lon: longtitude,
      lat: latitude,
      cnt: 50
    };
    return this.sendRequest(customParams, 'find');
  }

  sendRequest(customParams, directory) {
    let params = {...this.mainParams, ...customParams};
    let url = `${environment.weatherServiceUrl}${directory}`;
    return this.http.get(url, { params: params });
  }

}
